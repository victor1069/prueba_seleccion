import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DivOpcionesComponent } from './div-opciones.component';

describe('DivOpcionesComponent', () => {
  let component: DivOpcionesComponent;
  let fixture: ComponentFixture<DivOpcionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DivOpcionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DivOpcionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
