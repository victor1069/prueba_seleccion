import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DivImgBannerComponent } from './div-img-banner.component';

describe('DivImgBannerComponent', () => {
  let component: DivImgBannerComponent;
  let fixture: ComponentFixture<DivImgBannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DivImgBannerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DivImgBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
