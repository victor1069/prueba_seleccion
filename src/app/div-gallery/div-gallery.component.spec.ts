import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DivGalleryComponent } from './div-gallery.component';

describe('DivGalleryComponent', () => {
  let component: DivGalleryComponent;
  let fixture: ComponentFixture<DivGalleryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DivGalleryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DivGalleryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
