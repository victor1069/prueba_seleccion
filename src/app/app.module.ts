import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { DivImgBannerComponent } from './div-img-banner/div-img-banner.component';
import { FormBusquedaComponent } from './form-busqueda/form-busqueda.component';
import { DivOpcionesComponent } from './div-opciones/div-opciones.component';
import { DivGalleryComponent } from './div-gallery/div-gallery.component';
import { DivPreferenciasComponent } from './div-preferencias/div-preferencias.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';
import { DivFooterComponent } from './div-footer/div-footer.component';
// import { PruebaService } from './prueba.service';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    DivImgBannerComponent,
    FormBusquedaComponent,
    DivOpcionesComponent,
    DivGalleryComponent,
    DivPreferenciasComponent,
    DivFooterComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
    ModalModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
